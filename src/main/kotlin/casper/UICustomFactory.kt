package casper

import casper.geometry.Vector2i
import casper.gui.UIScene
import casper.gui.component.panel.UIPanel
import casper.gui.component.tab.UITab
import casper.gui.layout.Layout

class UICustomFactory {
	companion object {
		fun createTab(uiScene: UIScene, caption: String): UITab {
			val content = UIPanel.create(uiScene).node
			return UITab.createWithText(uiScene, caption, Vector2i(110, 30), content)
		}
	}
}