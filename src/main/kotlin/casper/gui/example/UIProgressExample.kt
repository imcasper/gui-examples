package casper.gui.example

import casper.UICustomFactory
import casper.gui.UIScene
import casper.gui.component.UITimer
import casper.gui.component.bar.UIProgressBar
import casper.gui.component.scroll.ScrollLogic
import casper.gui.layout.Orientation
import casper.gui.layout.OrientationLayout

class UIProgressExample(uiScene: UIScene) {
	val tab = UICustomFactory.createTab(uiScene, "progress")

	init {
		val node = tab.content
		node.layout = OrientationLayout(Orientation.VERTICAL)
		node.setSize(600, 500)

		val line1 = UIProgressBar(ScrollLogic.create(uiScene, 10.0, 0.0, 100.0), false)
		node += line1.node

		val line2 = UIProgressBar(ScrollLogic.create(uiScene, 90.0, 0.0, 100.0), false)
		node += line2.node

		val line3 = UIProgressBar(ScrollLogic.create(uiScene, 90.0, 0.0, 100.0), false)
		UITimer.createRepeated(line3.node, 100) {
			line3.logic.setValue(line3.logic.onValue.value)
			if (line3.logic.onValue.value >= line3.logic.onMax.value) line3.logic.setValue(0.0)
		}
		node += line3.node


//		val window2 = UIPanel.create(uiScene).node.setSize(200, 100)
//		window2.setPosition(400, 200)
//		UIMoveByPointer(window2, window2, window2)
//		UIContentToNodeLimit(uiScene.root, window2)
//		window2 += UIText.create(uiScene, "Move inside PAGE").node.setSize(200, 100)
//		node += window2
	}
}