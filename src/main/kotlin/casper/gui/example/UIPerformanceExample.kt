package casper.gui.example

import casper.UICustomFactory
import casper.geometry.Vector2d
import casper.gui.UIScene
import casper.gui.component.text.FontProperty
import casper.gui.component.text.TextColorProperty
import casper.gui.component.text.UIText
import casper.gui.layout.*
import casper.types.BLUE
import casper.types.setAlpha

class UIPerformanceExample(uiScene: UIScene) {
	val tab = UICustomFactory.createTab(uiScene, "performance")

	init {
		val node = tab.content
		node.layout = Layout.HORIZONTAL

		for (x in 1..20) {
			val panel = uiScene.createNode()
			panel.layout = OrientationLayout(Orientation.VERTICAL, Align.CENTER, Vector2d.ZERO)
			node += panel
			for (y in 1..80) {
				val textNode = UIText(uiScene.createNode(), "pos $x; $y").node
				textNode.setSize(35, 6)
				panel += textNode
				textNode.propertyCollection.set(FontProperty("Arial", 8, false, false, false))
				textNode.propertyCollection.set(TextColorProperty(BLUE.setAlpha(1.0)))
			}
		}
	}
}