package casper.gui.example

import casper.UICustomFactory
import casper.gui.UIScene
import casper.gui.component.UIImage
import casper.gui.component.text.UIText
import casper.gui.layout.Layout
import casper.types.*

class UIImageExample(uiScene: UIScene) {
	val tab = UICustomFactory.createTab(uiScene, "images")

	val paperScale = ScaleTexture(Texture("paper.png"), 0.0)
	val paperScale9 = Scale9Texture(Texture("paper.png"), 0.0, Padding(60, 60, 260, 120))

	init {
		val node = tab.content
		node.layout = Layout.VERTICAL

		node += UIText.create(uiScene, "scale").node.setSize(200, 30)
		node += UIImage.create(uiScene, paperScale).node.setSize(500, 300)

		node += UIText.create(uiScene, "scale9").node.setSize(200, 30)
		node += UIImage.create(uiScene, paperScale9).node.setSize(500, 300)

		node += UIText.create(uiScene, "LIME").node.setSize(200, 30)
		node += UIImage.create(uiScene, ColorFill(LIME.setAlpha(1.0))).node.setSize(500, 100)

		node += UIText.create(uiScene, "LIME (alpha = 0.5)").node.setSize(200, 30)
		node += UIImage.create(uiScene, ColorFill(LIME.setAlpha(0.5))).node.setSize(500, 100)
	}
}