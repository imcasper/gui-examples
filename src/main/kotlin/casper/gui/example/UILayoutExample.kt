package casper.gui.example

import casper.UICustomFactory
import casper.geometry.Vector2d
import casper.gui.UIScene
import casper.gui.component.button.UIButton
import casper.gui.component.button.UIButtonWithLabel
import casper.gui.component.panel.UIPanel
import casper.gui.component.text.UIText
import casper.gui.layout.*

class UILayoutExample(uiScene: UIScene) {
	val tab = UICustomFactory.createTab(uiScene, "layout")


	init {
		val node = tab.content
		node.layout = null
		node.setSize(800, 600)

		var border = 5.0
		var gap = 5.0
		var orientation = Orientation.VERTICAL
		var align = Align.CENTER

		val panelA = UIPanel.create(uiScene).node
		panelA.setPosition(0, 0)
		panelA.layout = OrientationLayout(orientation, align, Vector2d(border), Vector2d(gap))
		panelA += UIText.create(uiScene, "Direction layout test").node.setSize(140, 20)
		panelA += UIButton.createWithText(uiScene, "vertical") { orientation = Orientation.VERTICAL; panelA.layout = OrientationLayout(orientation, align, Vector2d(border), Vector2d(gap)) }.node.setSize(110, 30)
		panelA += UIButton.createWithText(uiScene, "horizontal") { orientation = Orientation.HORIZONTAL; panelA.layout = OrientationLayout(orientation, align, Vector2d(border), Vector2d(gap)) }.node.setSize(110, 30)
		panelA += UIButton.createWithText(uiScene, "align min") { align = Align.MIN; panelA.layout = OrientationLayout(orientation, align, Vector2d(border), Vector2d(gap)) }.node.setSize(120, 30)
		panelA += UIButton.createWithText(uiScene, "align center") { align = Align.CENTER; panelA.layout = OrientationLayout(orientation, align, Vector2d(border), Vector2d(gap)) }.node.setSize(120, 30)
		panelA += UIButton.createWithText(uiScene, "align max") { align = Align.MAX; panelA.layout = OrientationLayout(orientation, align, Vector2d(border), Vector2d(gap)) }.node.setSize(120, 30)
		panelA += UIButton.createWithText(uiScene, "border +") { ++border;panelA.layout = OrientationLayout(orientation, align, Vector2d(border), Vector2d(gap)) }.node.setSize(70, 40)
		panelA += UIButton.createWithText(uiScene, "border -") { --border;panelA.layout = OrientationLayout(orientation, align, Vector2d(border), Vector2d(gap)) }.node.setSize(70, 40)
		panelA += UIButton.createWithText(uiScene, "gap +") { ++gap;panelA.layout = OrientationLayout(orientation, align, Vector2d(border), Vector2d(gap)) }.node.setSize(50, 30)
		panelA += UIButton.createWithText(uiScene, "gap -") { --gap;panelA.layout = OrientationLayout(orientation, align, Vector2d(border), Vector2d(gap)) }.node.setSize(50, 30)
		node += panelA

		val panelC = UIPanel.create(uiScene).node
		panelC.setPosition(0, 300)
		panelC.layout = Layout.BORDER
		panelC += UIText.create(uiScene, "Border layout test\nsome text for line").node.setSize(160, 40)
		node += panelC

		val panelD = UIPanel.create(uiScene).node
		panelD.setPosition(400, 300).setSize(400, 300)
		panelD.layout = MultiLayout(Layout.LEFT_TOP, Layout.CENTER_TOP, Layout.RIGHT_TOP, Layout.LEFT_CENTER, Layout.CENTER_CENTER, Layout.RIGHT_CENTER, Layout.LEFT_BOTTOM, Layout.CENTER_BOTTOM, Layout.RIGHT_BOTTOM)
		panelD += UIText.create(uiScene, "left-top").node.setSize(95, 20)
		panelD += UIText.create(uiScene, "center-top").node.setSize(95, 20)
		panelD += UIText.create(uiScene, "right-top").node.setSize(95, 20)
		panelD += UIText.create(uiScene, "left-center").node.setSize(95, 20)
		panelD += UIText.create(uiScene, "center-center").node.setSize(95, 20)
		panelD += UIText.create(uiScene, "right-center").node.setSize(95, 20)
		panelD += UIText.create(uiScene, "left-bottom").node.setSize(95, 20)
		panelD += UIText.create(uiScene, "center-bottom").node.setSize(95, 20)
		panelD += UIText.create(uiScene, "right-bottom").node.setSize(95, 20)
		node += panelD


	}
}