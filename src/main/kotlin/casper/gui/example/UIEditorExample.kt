package casper.gui.example

import casper.UICustomFactory
import casper.gui.UIScene
import casper.gui.component.text.TextFormatProperty
import casper.gui.component.text.UIText
import casper.gui.component.text.UITextInput
import casper.gui.layout.Layout

class UIEditorExample(uiScene: UIScene) {
	val tab = UICustomFactory.createTab(uiScene, "editor")

	init {
		val node = tab.content
		node.layout = Layout.VERTICAL

		val txtInfo = UIText.create(uiScene, "")
		txtInfo.node.propertyCollection.set(TextFormatProperty(false, 0.0, true))
		node += txtInfo.node


		node += UITextInput.create(uiScene, "first text") { text ->
			txtInfo.text = "You entered:\n\"$text\""
		}.node.setSize(200, 50)
	}
}