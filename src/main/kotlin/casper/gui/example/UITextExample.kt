package casper.gui.example

import casper.UICustomFactory
import casper.gui.UIScene
import casper.gui.component.text.TextFormatProperty
import casper.gui.component.text.UIText
import casper.gui.layout.Orientation
import casper.gui.layout.OrientationLayout

class UITextExample(uiScene: UIScene) {
	val tab = UICustomFactory.createTab(uiScene, "text")

	init {
		val node = tab.content
		node.layout = OrientationLayout(Orientation.VERTICAL)
		node.setSize(600, 500)

		val line1 = UIText(uiScene.createNode(), "Line1")
		line1.formatProperty = TextFormatProperty(false, 0.0, true)
		node+= line1.node

		val line2 = UIText(uiScene.createNode(), "Line2")
		line2.formatProperty = TextFormatProperty(false, 0.0, true)
		node+= line2.node

		val line3 = UIText(uiScene.createNode(), "Line3")
		line3.formatProperty = TextFormatProperty(false, 0.0, true)
		node+= line3.node

//		val window2 = UIPanel.create(uiScene).node.setSize(200, 100)
//		window2.setPosition(400, 200)
//		UIMoveByPointer(window2, window2, window2)
//		UIContentToNodeLimit(uiScene.root, window2)
//		window2 += UIText.create(uiScene, "Move inside PAGE").node.setSize(200, 100)
//		node += window2
	}
}