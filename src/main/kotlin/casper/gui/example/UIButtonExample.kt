package casper.gui.example

import casper.UICustomFactory
import casper.gui.UIScene
import casper.gui.component.UIComponent
import casper.gui.component.UIImage
import casper.gui.component.button.UIButton
import casper.gui.component.button.UIButtonWithImage
import casper.gui.component.button.UIButtonWithLabel
import casper.gui.component.button.setText
import casper.gui.component.text.UIText
import casper.gui.component.toggle.UIToggle
import casper.gui.component.toggle.UIToggleWithImage
import casper.gui.component.toggle.UIToggleWithLabel
import casper.gui.component.toggle.setText
import casper.gui.layout.Layout
import casper.types.BLUE
import casper.types.ColorFill
import casper.types.Texture
import casper.types.setAlpha

class UIButtonExample(uiScene: UIScene) {
	val tab = UICustomFactory.createTab(uiScene, "buttons")

	init {
		val node = tab.content
		node.layout = Layout.VERTICAL

		val txtInfo = UIText.create(uiScene, "")
		txtInfo.node.setSize(200, 50)
		node += txtInfo.node

		val buttonWithLabel = UIButton.createWithText(uiScene, "btn")
		node += buttonWithLabel.node.setSize(100, 30)

		var clickAmount1 = 0
		buttonWithLabel.logic.onClick.then {
			txtInfo.text = "You pressed\nbutton with label"
			buttonWithLabel.setText("click ${++clickAmount1}")
		}

		val emptyButton = UIButton.create(uiScene, UIComponent(uiScene.createNode()))
		node += emptyButton.node
		emptyButton.node.setSize(100, 30)
		emptyButton.logic.onClick.then {
			txtInfo.text = "You pressed\nempty button"
		}

		val complex = uiScene.createNode()
		complex.layout = Layout.VERTICAL
		complex += UIText.create(uiScene, "it is complex button").node.setSize(150, 20)
		complex += UIImage.create(uiScene, ColorFill(BLUE.setAlpha(1.0))).node.setSize(80, 40)
		complex += UIButton.createWithText(uiScene, "sub-button").node.setSize(120, 40)

		val complexButton = UIButton.create(uiScene, UIComponent(complex))
		node += complexButton.node
		complexButton.node.setSize(180, 120)
		complexButton.logic.onClick.then {
			txtInfo.text = "You pressed\ncomplex button"
		}

		val buttonWithImage = UIButton.createWithImage(uiScene, Texture("icon.png"))
		node += buttonWithImage.node
		buttonWithImage.node.setSize(100, 100)
		buttonWithImage.logic.onClick.then {
			txtInfo.text = "You pressed\nbutton with image"
		}

		val toggleWithImage = UIToggle.createWithImage(uiScene, Texture("icon.png"))
		node += toggleWithImage.node
		toggleWithImage.node.setSize(120, 60)
		toggleWithImage.logic.switch.then {
			txtInfo.text = "You pressed\ntoggle with image"
		}

		val toggle = UIToggle.createWithText(uiScene, "toggle")
		toggle.node.setSize(120, 60)
		var clickAmount = 0
		toggle.logic.switch.then { on ->
			txtInfo.text = "You pressed\ntoggle with label"
			toggle.setText("click:${clickAmount++}\n${if (on) "on" else "off"}")
		}
		node += toggle.node
	}
}