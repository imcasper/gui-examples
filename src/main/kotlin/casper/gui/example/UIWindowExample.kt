package casper.gui.example

import casper.UICustomFactory
import casper.gui.UIScene
import casper.gui.component.UIMoveByPointer
import casper.gui.component.panel.UIPanel
import casper.gui.component.text.UIText

class UIWindowExample(uiScene: UIScene) {
	val tab = UICustomFactory.createTab(uiScene, "window")

	init {
		val node = tab.content
		node.isHitByArea = false
		node.layout = null
		node.setSize(600, 500)

		val window = UIPanel.create(uiScene).node.setSize(200, 100)
		window.setPosition(100, 200)
		UIMoveByPointer(window, window, window)
//		UIContentToNodeLimit(tab.content, window)
		window += UIText.create(uiScene, "Move inside TAB").node.setSize(200, 100)
		node += window

//		val window2 = UIPanel.create(uiScene).node.setSize(200, 100)
//		window2.setPosition(400, 200)
//		UIMoveByPointer(window2, window2, window2)
//		UIContentToNodeLimit(uiScene.root, window2)
//		window2 += UIText.create(uiScene, "Move inside PAGE").node.setSize(200, 100)
//		node += window2
	}
}