package casper

import casper.gui.UIScene
import casper.gui.component.bar.ProgressBarStyle
import casper.gui.component.box.ScrollBoxBorderProperty
import casper.gui.component.button.ButtonBackgroundStyle
import casper.gui.component.button.ButtonStyle
import casper.gui.component.panel.PanelProperty
import casper.gui.component.scroll.ScrollStyle
import casper.gui.component.tab.TabMenuStyle
import casper.gui.component.text.TextInputProperty
import casper.gui.component.toggle.ToggleStyle
import casper.gui.setProperty
import casper.types.Color4d
import casper.types.Padding
import casper.types.Scale9Texture
import casper.types.Texture

fun setupProperties(uiScene: UIScene) {
	val pad30x30 = Padding(10, 10, 20, 20)
	val pad62x62 = Padding(4, 4, 59, 59)
	val pad64x64 = Padding(4, 4, 60, 60)
	val pad70x70 = Padding(8, 8, 62, 62)

	val panel = Scale9Texture(Texture("style/panel/panel.png"), 0.0, Padding(5, 5, 65, 65))
	val panelStyle = PanelProperty(panel)
	uiScene.setProperty(panelStyle)

	val buttonBackStyle = ButtonBackgroundStyle(
			Scale9Texture(Texture("style/button/btnOut.png"), 0.0, pad64x64),
			Scale9Texture(Texture("style/button/btnOver.png"), 0.0, pad64x64),
			Scale9Texture(Texture("style/button/btnPressOut.png"), 0.0, pad64x64),
			Scale9Texture(Texture("style/button/btnPressOver.png"), 0.0, pad64x64),
			Scale9Texture(Texture("style/button/btnDisabled.png"), 0.0, pad64x64))

	val selectedButtonBackStyle = ButtonBackgroundStyle(
			Scale9Texture(Texture("style/button/btnSelectedOut.png"), -3.0, pad70x70),
			Scale9Texture(Texture("style/button/btnSelectedOver.png"), -3.0, pad70x70),
			Scale9Texture(Texture("style/button/btnSelectedPressOut.png"), -3.0, pad70x70),
			Scale9Texture(Texture("style/button/btnSelectedPressOver.png"), -3.0, pad70x70),
			Scale9Texture(Texture("style/button/btnDisabled.png"), -3.0, pad64x64))

	val scrollBackStyle = ButtonBackgroundStyle(
			Scale9Texture(Texture("style/button/btnSmallOut.png"), 0.0, pad62x62),
			Scale9Texture(Texture("style/button/btnSmallOver.png"), 0.0, pad62x62),
			Scale9Texture(Texture("style/button/btnSmallPressOut.png"), 0.0, pad62x62),
			Scale9Texture(Texture("style/button/btnSmallPressOver.png"), 0.0, pad62x62),
			Scale9Texture(Texture("style/button/btnSmallDisabled.png"), 0.0, pad70x70))

	val buttonStyle = ButtonStyle(buttonBackStyle, 6.0)
	uiScene.setProperty(buttonStyle)

	val selectedButtonStyle = ButtonStyle(selectedButtonBackStyle, 4.0)

	uiScene.setProperty(ScrollBoxBorderProperty(
			Scale9Texture(Texture("style/box/boxLeft.png"), 0.0, pad30x30),
			Scale9Texture(Texture("style/box/boxRight.png"), 0.0, pad30x30),
			Scale9Texture(Texture("style/box/boxTop.png"), 0.0, pad30x30),
			Scale9Texture(Texture("style/box/boxBottom.png"), 0.0, pad30x30))
	)

	uiScene.setProperty(TextInputProperty(Color4d(0.0, 0.0, 0.0, 0.6),
			Scale9Texture(Texture("style/input/normal.png"), 0.0, pad70x70),
			Scale9Texture(Texture("style/input/focused.png"), 0.0, pad70x70)))

	uiScene.setProperty(ScrollStyle(20.0, 30.0, 10.0, scrollBackStyle, buttonBackStyle))

	uiScene.setProperty(ToggleStyle(selectedButtonStyle, buttonStyle))
	uiScene.setProperty(TabMenuStyle(panel, null))

	uiScene.setProperty(ProgressBarStyle(20.0,
			Scale9Texture(Texture("style/bar/back.png"), 0.0, pad70x70),
			Scale9Texture(Texture("style/bar/progress.png"), 0.0, pad70x70)))

}