package casper

import BABYLON.extension.createScene
import BABYLON.extension.runRenderLoop
import babylon.BabylonUIScene
import casper.collection.observableSetOf
import casper.gui.UIScene
import casper.gui.component.scroll.UIScrollBox
import casper.gui.component.tab.TabMenuStyle
import casper.gui.component.tab.UITabMenu
import casper.gui.example.*
import casper.gui.layout.Direction
import casper.gui.layout.Layout
import casper.gui.util.BooleanGroupRule
import org.w3c.dom.Worker


fun main() {
	val scene = createScene("mainCanvas", false)
	scene.createDefaultCameraOrLight(true, true, true)
	scene.createDefaultEnvironment()

	createUI(BabylonUIScene(scene))

	scene.runRenderLoop()
}

fun createUI(uiScene: UIScene) {
	setupProperties(uiScene)

	val tabs = observableSetOf(
			UIImageExample(uiScene).tab,
			UILayoutExample(uiScene).tab,
			UIButtonExample(uiScene).tab,
			UIEditorExample(uiScene).tab,
			UIScrollExample(uiScene).tab,
			UITabExample(uiScene).tab,
			UIWindowExample(uiScene).tab,
			UITextExample(uiScene).tab,
			UIProgressExample(uiScene).tab,
			UIPerformanceExample(uiScene).tab
	)

	val tabMenu = UITabMenu.create(uiScene, toggleSide = Direction.TOP, rule = BooleanGroupRule.ONLY_ONE_TRUE, tabs = tabs)

	val scrollBox = UIScrollBox.create(uiScene)

//	(scrollBox.node as BabylonUINode).source.isPointerBlocker = false
//	(scrollBox.node as BabylonUINode).source.isHitTestVisible = true

	scrollBox.content.layout = Layout.BORDER
	scrollBox.content.isHitByArea = false
	scrollBox.content += tabMenu.node

	uiScene.root.layout = Layout.STRETCH
	uiScene.root += scrollBox.node
}
